<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  function () {
    return view('welcome');
});

Auth::routes();

Route::get('/forum', 'ForumController@index')->name('forum');

Route::resource('discussion', 'DiscussionsController');

Route::get('/discussion/{slug}/', [
    'uses'   =>  'DiscussionsController@show',
    'as'     =>  'discussion'
]);

Route::get('/channel/{slug}/', [
    'uses'   =>  'ForumController@channel',
    'as'     =>  'channel'
]);

//Social auth routes
Route::get('socialauth/{provider}', 'Auth\SocialAuthController@redirectToProvider');
Route::get('socialauth/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback');



Route::group(['middleware' => 'auth'], function(){
    Route::resource('channels', 'ChannelsController');
    Route::post('channel/delete', 'ChannelsController@destroy');
    Route::post('channel/update', 'ChannelsController@update');
});

Route::post('reply/store', 'ReplyController@store');

Route::post('like/store', 'LikesController@store');

Route::post('/reply/unlike', [
    'uses' => 'LikesController@destroy',
    'as' => 'reply.unlike'
]);

Route::get('/edit/profile', [
   'uses'   =>  'ProfileController@edit',
   'as'     =>  'edit.profile'
]);

Route::post('/edit/profile/save', [
    'uses'   =>  'ProfileController@store',
    'as'     =>  'profile.store'
]);

Route::get('/changePassword','ChangePasswordController@showChangePasswordForm')->name('changePassword.page');
Route::post('/changePassword','ChangePasswordController@changePassword')->name('changePassword');

Route::get('/subscribe/{id}','SubscribesController@subscribe')->name('subscribe');
Route::get('/unsubscribe/{id}','SubscribesController@unSubscribe')->name('unSubscribe');

Route::get('/results', function(){
   $dis = \App\Discussion::where('title', 'like', '%' . request('search') . '%')->get();

   return view('result')->with('dis', $dis);
});

Route::get('/best-answer/{id}', [
    'uses'  =>  'ReplyController@bestAnswer',
    'as'    =>  'best.reply'
]);

//Route::get('/testing', function(){
//   return view('testing');
//});


Route::get('/notifications', function(){
   return view('noti')->with('notis', \App\Notification::all());
})->name('noti');
