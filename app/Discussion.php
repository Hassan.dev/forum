<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Discussion extends Model
{
    protected $fillable = ['user_id', 'channel_id', 'title', 'content', 'slug'];

    public function channel()
    {
        return $this->belongsTo('App\Channel');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    public function subscribers()
    {
        return $this->hasMany('App\Subscribe');
    }

    public function is_subscribe()
    {
        $id = Auth::id();

        $subscribers = array();

        foreach($this->subscribers as $s):
            array_push($subscribers, $s->user_id);
        endforeach;

        if(in_array($id, $subscribers)):
            return true;
            else:
                return false;
        endif;
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }
}
