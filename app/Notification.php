<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Notification extends Model
{
    protected $fillable = ['reply_id', 'user_id', 'discussion_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function discussion()
    {
        return $this->belongsTo('App\Discussion');
    }

    public function reply()
    {
        return $this->belongsTo('App\Reply');
    }

    public function noti()
    {
        $id = Auth::id();
        $notis = array();

        foreach($this->user as $noti):
            array_push($notis, $noti->notifications->user_id);
        endforeach;

        if(in_array($id, $notis)):
                return true;
        else:
            return false;
        endif;

    }

}
