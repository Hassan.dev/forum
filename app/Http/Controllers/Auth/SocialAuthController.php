<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\User;

class SocialAuthController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
//        $user = Socialite::driver($provider)->user();
//
////        $authUser=User::firstOrNew(['provider_id' => $user->id]);
////
////        $authUser->name=$user->nickname;
////        $authUser->email=$user->email;
////        $authUser->provider_id=$user->id;
////        $authUser->avatar=$user->avatar;
////        $authUser->provider=$provider;
////
////        $authUser->save();
////
////
////        auth()->login($authUser);
////
////        return redirect('/forum');
//
//        dd($user);


        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }

        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->provider_id     = $user->id;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->avatar          =$user->avatar;
            $newUser->provider        =$provider;

            $newUser->save();
            auth()->login($newUser, true);
        }
        return redirect()->to('/forum');



    }

}
