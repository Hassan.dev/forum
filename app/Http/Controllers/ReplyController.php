<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Discussion;
use App\User;
use Auth;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reply = new Reply();

        $reply->user_id = Auth::id();
        $reply->discussion_id = $request->saveId;
        $reply->content = $request->reply;

        $reply->save();

        $d = Discussion::find($request->saveId);

        $subscribers = array();

        foreach($d->subscribers as $subscriber):
            array_push($subscribers, User::find($subscriber->user_id));
        endforeach;

        Notification::send($subscribers, new \App\Notifications\AddSubscription($d));

        $noti = new \App\Notification();
        $noti->discussion_id = $request->saveId;
        $noti->user_id = Auth::id();

        $noti->save();


        return response()->json('123');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function unlike(Request $request, $id)
    {
        $id = $request->replyId;
        $unlike = Like::where('reply_id', $id)->where('user_id', Auth::id())->first();
        $unlike->delete();
        return redirect()->back();
    }

    public function bestAnswer($id)
    {
        $bestReply = Reply::find($id);

        $bestReply->best_reply = 1;

        $bestReply->save();

        return redirect()->back();
    }
}
