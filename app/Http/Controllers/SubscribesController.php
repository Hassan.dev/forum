<?php

namespace App\Http\Controllers;

use App\Subscribe;
use Illuminate\Http\Request;
use Auth;

class SubscribesController extends Controller
{
    public function subscribe($id)
    {

        Subscribe::create([
           'discussion_id' => $id,
            'user_id'   =>  Auth::id()
        ]);


        return redirect()->back();
    }

    public function unSubscribe($id)
    {
        $subscribe = Subscribe::where('discussion_id', $id)->where('user_id', Auth::id())->first();

        $subscribe->delete();

        return redirect()->back();
    }
}
