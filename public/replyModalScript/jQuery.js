$(document).ready(function(){

    $('#upload_form').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: "/reply/store",
            method: "POST",
            data: new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,

            success: function (data) {
                toastr.success('Reply added successfully.');
                $('#exampleModal').modal('hide');
                $('.container1').load(location.href + (' .container1'));
            }

        });
    });

    $('#new').click(function(){   //reply controller
        var id = $('.disId').val();
        $('.saveId').val(id);
    });

    $('.like').click(function(){
        var replyId = $(this).data('id');

        $.post('/like/store', {'replyId':replyId, '_token':$('input[name=_token]').val(), function(){
                $('.container1').load(location.href + (' .container1'));
            }

        });
        $('.container1').load(location.href + (' .container1'));
    });


    $('.unlike').click(function(){
        var replyId = $(this).data('id');

        $.post('/reply/unlike', {'replyId':replyId, '_token':$('input[name=_token]').val(), successFunction(data){
            }
        });
        $('.container1').load(location.href + (' .container1'));
    });

});