$(document).ready(function(){

    $(document).on('click', '#new', function(event){
        event.preventDefault()
        $('#header').html('Create a new discussion');
        $('.editTitle').val("");
        $('.editContent').val("");
        $('#add').show();
        $('#delete').hide();
        $('#update').hide();



    });

    $('#upload_form').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: "home",
            method: "POST",
            data: new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,

            success: function (data) {
                toastr.success('Discussion created successfully.');
                $('#exampleModal').modal('hide');
                $('.container1').load(location.href + (' .container1'));
                console.log(data);
            }

        });
    });

    //edit modal
    $(document).on('click', '.entry', function(event){
        event.preventDefault();

        var title = $(this).find('.title').text();
        var content = $(this).find('.content').text();
        var id = $(this).find('.id').text();


        $('#header').html('Edit category: <b>' + title);
        $('#add').hide();
        $('#delete').show();
        $('#update').show();


        $('.editTitle').val(title);
        $('.editContent').val(content);
        $('.editId').val(id);

    });


});

document.getElementById('new').onclick = (function() {
    document.getElementsByTagName('span')[0].innerHTML = 'ask properly!';
    return false;
});
