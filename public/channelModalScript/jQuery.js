$(document).ready(function(){

    $(document).on('click', '#new', function(event){
        $('#header').html('Create a new category');
        $('.editTitle').val("");
        $('#add').show();
        $('#delete').hide();
        $('#update').hide();
    });

    $('#upload_form').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: "channels",
            method: "POST",
            data: new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,

            success: function (data) {
                toastr.success('Channel created successfully.')
                $('#exampleModal').modal('hide');
                $('.container1').load(location.href + (' .container1'));
            }

        });
    });

    //edit modal
    $(document).on('click', '.entry', function(event){
        event.preventDefault();

        var title = $(this).find('.title').text();
        var id = $(this).find('.id').text()

        $('#header').html('Edit category: <b>' + title);
        $('#add').hide();
        $('#delete').show();
        $('#update').show();

        $('.editTitle').val(title);
        $('.editId').val(id);

    });

    $('#delete').click(function(event){
        event.preventDefault()
        var id = $('.editId').val();

        $.post('channel/delete', {'id':id, '_token':$('input[name=_token]').val(), function(){

            toastr.error('Channel deleted.');
            }
        });
        $('.container1').load(location.href + (' .container1'));
    });

    $('#update').click(function(event){
       event.preventDefault();
       var id = $('.editId').val();
       var title = $('.editTitle').val();

       $.post('channel/update', {'id':id, 'title':title, '_token':$('input[name=_token]').val(), function(){
               toastr.info('Channel updated.');
           }
       });
        $('.container1').load(location.href + (' .container1'));

    });


    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }


});

