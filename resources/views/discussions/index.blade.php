@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form action="{{route('discussion.store')}}" method="post" id="upload_form" enctype="multipart/form-data">

                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">

                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="title of discussion"><br>
                        <label for="channels">Pick a channel:</label>
                        <select name="category_id" class="form-control">
                            @foreach($channels as $channel)
                                <option value="{{$channel->id}}">{{$channel->title}}</option>
                            @endforeach
                        </select><br>

                        <label for="content">Ask a question</label>
                        <textarea id="content1" name="content1" class="form-control gg" cols="20" rows="15"></textarea>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="add" class="btn btn-primary pull-right">Create discussion</button>
                </div>

            </form>
        </div>
    </div>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>




    {{--<script src="{{ asset('/discussionModalScript/jQuery.js') }}"></script>--}}

@endsection

