@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <div class="container1">
    <div class="card">
        <div class="card-header">
            <img src="{{$d->user->avatar}}" width="40px" height="43px">&nbsp;&nbsp;
            <span>{{$d->user->name}}, <b>{{$d->created_at->diffForHumans()}}</b></span>

            <input type="hidden" class="disId" value="{{$d->id}}">
        </div>

        <div class="card-body">
            <h4 class="text-center">
                {{$d->title}}
            </h4>
            <hr>
            <p class="text-center">
                {{$d->content}}
            </p>

            <hr>

            @if($bestReply)
                <div class="text-center" style="padding: 40px">
                    <h4 class="text-center">Best Answer</h4>
                    <div class="card">
                        <div class="card-headerp-3 mb-2" style="background: #DFF0D8">
                            <img src="{{$bestReply->user->avatar}}" width="40px" height="43px">&nbsp;&nbsp;
                            <span>{{$bestReply->user->name}}</span>
                        </div>

                        <div class="card-body">
                            {{$bestReply->content}}
                        </div>
                    </div>
                </div>
            @endif


        </div>

        <div class="card-footer">
            {{$d->replies->count()}} Replies
            <a href="{{route('channel', ['slug'=>$d->channel->slug])}}" class="float-right btn btn-outline-secondary">{{$d->channel->title}}</a>
        </div>
    </div><br>


    @foreach($d->replies as $r)
        <div class="card">
            <div class="card-header">
                <img src="{{$r->user->avatar}}" width="40px" height="43px">&nbsp;&nbsp;
                <span>{{$r->user->name}}</span>

                @if(!($bestReply))
                    @if(Auth::id() == $d->user->id)
                        <a href="{{route('best.reply', ['id' => $r->id])}}" class="float-right btn btn-xs btn-info">Mart as best answer</a>
                    @endif
                @endif

                <input class="replyId" type="hidden" value="{{$r->id}}">
            </div>

            <div class="card-body">
                <p class="text-center">
                    {{$r->content}}
                </p>
            </div>

            <div class="card-footer">
                {{csrf_field()}}
                @if($r->is_liked_by_auth_user())
                    <a class="btn btn-danger unlike" data-id="{{$r->id}}">Unlike</a>
                @else
                    <a class="btn btn-success like" data-id="{{$r->id}}">Like</a>
                @endif
                <span class="badge badge-pill badge-primary">{{$r->likes->count()}}</span> Likes
            </div>
        </div>
    @endforeach
        </div>

        @if(Auth::check())
            (Add new reply)

            <a href="" id="new" class="pull-right" data-toggle="modal" data-target="#exampleModal">
                <i class="fas fa-plus"></i>
            </a>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

                <form  method="post" id="upload_form" enctype="multipart/form-data">

                    <div class="modal-dialog" role="document">

                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="header">Add a new reply</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ csrf_field() }}
                                <div class="form-group">

                                    <textarea id="reply" name="reply" class="form-control" cols="20" rows="15"></textarea>
                                    <input type="hidden" name="saveId" class="saveId" value="">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="add" class="btn btn-primary pull-right">Add</button>
                                <button type="submit" id="delete" class="btn btn-danger pull-right" style="display: none" data-dismiss="modal">Delete</button>
                                <button type="submit" id="update" class="btn btn-success pull-right" style="display: none" data-dismiss="modal">Save Changes</button>
                            </div>

                        </div>
                    </div>
                </form>

            </div>

            <script src="{{ asset('/replyModalScript/jQuery.js') }}"></script>

        @else
                Login to add a reply
        @endif

@endsection

