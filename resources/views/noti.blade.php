@extends('layouts.app')



@section('content')

    @foreach($notis as $noti)

        @if((Auth::id() == $noti->discussion->user_id))

            {{$noti->user->name}}&nbsp;comment on
            <a href="{{route('discussion', ['slug' => $noti->discussion->slug])}}">{{$noti->discussion->title}}</a>
            <hr>
            {{--{{$noti->user->notifications->count()}}--}}
        @endif

    @endforeach

@stop