@extends('layouts.app')

@section('content')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<div class="container1">
            <div class="card">
                <div class="card-header">
                    Channels
                    <a href="" id="new" class="pull-right" data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-plus"></i>
                    </a>
                </div>

                <div class="card-body">
                    <ul class="list-group">
                        @foreach($channels as $channel)
                            <li class="list-group-item entry" data-toggle="modal" data-target="#exampleModal">
                                <span class="title">{{$channel->title}}</span>
                                <span class="id" style="display: none">{{$channel->id}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>



    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

        <form  method="post" id="upload_form" enctype="multipart/form-data">

            <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="header">Create a new category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">

                            <tr><input type="text" name="title" id="title" class="form-control editTitle" placeholder="channel name"></tr>
                            <tr><input type="hidden" name="id" id="title" class="form-control editId"></tr>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="add" class="btn btn-primary pull-right">Add</button>
                        <button type="submit" id="delete" class="btn btn-danger pull-right" style="display: none" data-dismiss="modal">Delete</button>
                        <button type="submit" id="update" class="btn btn-success pull-right" style="display: none" data-dismiss="modal">Save Changes</button>
                    </div>

                </div>
            </div>
        </form>

    </div>


<script src="{{ asset('/channelModalScript/jQuery.js') }}"></script>

@stop
