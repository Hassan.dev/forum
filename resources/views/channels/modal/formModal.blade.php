@section('modal')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <form  method="post" id="upload_form" enctype="multipart/form-data">

        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="header">Create a new category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <div class="form-group">

                        <tr><input type="text" name="title" id="title" class="form-control editTitle" placeholder="channel name"></tr>
                        <tr><input type="hidden" name="id" id="title" class="form-control editId"></tr>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="add" class="btn btn-primary pull-right">Add</button>
                    <button type="submit" id="delete" class="btn btn-danger pull-right" style="display: none" data-dismiss="modal">Delete</button>
                    <button type="submit" id="update" class="btn btn-success pull-right" style="display: none" data-dismiss="modal">Save Changes</button>
                </div>

            </div>
        </div>
    </form>

</div>
@stop
