@extends('layouts.app')

@section('content')
    @foreach($dis as $d)
        <div class="card">
            <div class="card-header">
                <img src="{{$d->user->avatar}}" width="40px" height="43px">&nbsp;&nbsp;
                <span>{{$d->user->name}}</span>&nbsp;&nbsp;<b>{{$d->created_at->diffForHumans()}}</b>
                <a href="{{route('discussion', ['slug' => $d->slug])}}" class="btn btn-info" style="float: right">view</a>
            </div>

            <div class="card-body">
                <h4 class="text-center">
                    {{$d->title}}
                </h4>
                <p class="text-center">
                    {{str_limit($d->content, 50)}}
                </p>
            </div>

            <div class="card-footer">
                {{$d->replies->count()}} Replies
                <a href="{{route('channel', ['slug'=>$d->channel->slug])}}" class="float-right btn btn-outline-secondary">{{$d->channel->title}}</a>
            </div>

        </div>
    @endforeach
    <div class="text-center">
{{--        {{$d->links()}}--}}
    </div>
@endsection

