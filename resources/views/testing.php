<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>bootstrap4</title>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>
</head>
<body>


<a href="" id="new">
    <span>Create a new discussion</span>
</a>

<script>
    document.getElementById('new').onclick = (function() {
        document.getElementsByTagName('span')[0].innerHTML = 'ask properly!';
        return false;
    });
</script>

<style>
    @font-face {
        font-family: 'Pacifico';
        font-style: normal;
        font-weight: normal;
        src: local('Pacifico'), url('http://themes.googleusercontent.com/font?kit=fKnfV28XkldRW297cFLeqfesZW2xOQ-xsNqO47m55DA') format('truetype');
    }
    body {
        text-align: center;
    }

    #new {
        display: inline-block;
        margin-top: 20%;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        -webkit-box-shadow:    0 8px 0 #c5376d, 0 15px 20px rgba(0, 0, 0, .35);
        -moz-box-shadow: 0 8px 0 #c5376d, 0 15px 20px rgba(0, 0, 0, .35);
        box-shadow: 0 8px 0 #c5376d, 0 15px 20px rgba(0, 0, 0, .35);
        -webkit-transition: -webkit-box-shadow .1s ease-in-out;
        -moz-transition: -moz-box-shadow .1s ease-in-out;
        -o-transition: -o-box-shadow .1s ease-in-out;
        transition: box-shadow .1s ease-in-out;
        font-size: 25px;
        color: #fff;
    }

    #new span {
        display: inline-block;
        padding: 20px 30px;
        background-color: #ec528d;
        background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(hsla(338, 90%, 80%, .8)), to(hsla(338, 90%, 70%, .2)));
        background-image: -webkit-linear-gradient(hsla(338, 90%, 80%, .8), hsla(338, 90%, 70%, .2));
        background-image: -moz-linear-gradient(hsla(338, 90%, 80%, .8), hsla(338, 90%, 70%, .2));
        background-image: -o-linear-gradient(hsla(338, 90%, 80%, .8), hsla(338, 90%, 70%, .2));
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px;
        -webkit-box-shadow: inset 0 -1px 1px rgba(255, 255, 255, .15);
        -moz-box-shadow: inset 0 -1px 1px rgba(255, 255, 255, .15);
        box-shadow: inset 0 -1px 1px rgba(255, 255, 255, .15);
        font-family: 'Pacifico', Arial, sans-serif;
        line-height: 1;
        text-shadow: 0 -1px 1px rgba(175, 49, 95, .7);
        -webkit-transition: background-color .2s ease-in-out, -webkit-transform .1s ease-in-out;
        -moz-transition: background-color .2s ease-in-out, -moz-transform .1s ease-in-out;
        -o-transition: background-color .2s ease-in-out, -o-transform .1s ease-in-out;
        transition: background-color .2s ease-in-out, transform .1s ease-in-out;
    }

    #new:hover span {
        background-color: #ec6a9c;
        text-shadow: 0 -1px 1px rgba(175, 49, 95, .9), 0 0 5px rgba(255, 255, 255, .8);
    }

    #new:active, #new:focus {
        -webkit-box-shadow:    0 8px 0 #c5376d, 0 12px 10px rgba(0, 0, 0, .3);
        -moz-box-shadow: 0 8px 0 #c5376d, 0 12px 10px rgba(0, 0, 0, .3);
        box-shadow:    0 8px 0 #c5376d, 0 12px 10px rgba(0, 0, 0, .3);
    }

    #new:active span {
        -webkit-transform: translate(0, 4px);
        -moz-transform: translate(0, 4px);
        -o-transform: translate(0, 4px);
        transform: translate(0, 4px);
    }
</style>

</body>
</html