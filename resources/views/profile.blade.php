@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Edit your profile: <b>{{Auth::user()->name}}</b></div>

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            <form action="{{route('profile.store')}}" method="post" id="upload_form" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-group">

                        <label for="title">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{Auth::user()->name}}"><br>

                        <label for="title">Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{Auth::user()->email}}"><br>

                    </div>

                    <button type="submit" id="add" class="btn btn-primary pull-right">Save</button>

            </form>
        </div>
    </div>


@endsection

