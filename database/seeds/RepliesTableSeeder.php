<?php

use Illuminate\Database\Seeder;

class RepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = [
          'user_id' => '1',
          'discussion_id' => '1',
          'content' => 'this is first reply under this discussion.'
        ];

        $r2 = [
            'user_id' => '1',
            'discussion_id' => '2',
            'content' => 'this is 2nd reply under this discussion.'
        ];

        $r3 = [
            'user_id' => '1',
            'discussion_id' => '3',
            'content' => 'this is 3rd reply under this discussion.'
        ];

        $r4 = [
            'user_id' => '1',
            'discussion_id' => '4',
            'content' => 'this is 4th reply under this discussion.'
        ];

        $r5 = [
            'user_id' => '1',
            'discussion_id' => '5',
            'content' => 'this is 5th reply under this discussion.'
        ];

        \App\Reply::create($r1);
        \App\Reply::create($r2);
        \App\Reply::create($r3);
        \App\Reply::create($r4);
        \App\Reply::create($r5);
    }
}
