<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
           'name' => 'Hassan',
           'email' => 'abc@gmail.com',
           'password' => bcrypt('admin'),
           'admin'  =>  '1',
           'avatar' => asset('avatars/avatar.png')
        ]);
    }
}
