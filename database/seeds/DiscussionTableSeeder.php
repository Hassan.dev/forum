<?php

use Illuminate\Database\Seeder;

class DiscussionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $t1 = 'This is first discussion';
        $t2 = 'This is 2nd discussion';
        $t3 = 'This is 3rd discussion';
        $t4 = 'This is 4th discussion';
        $t5 = 'This is 5th discussion';

        $d1 = [

            'channel_id' => '1',
            'title' => $t1,
            'user_id' => '1',
            'content' => 'this is lorem ipsem text here',
            'slug' => str_slug($t1)
        ];

        $d2 = [

            'channel_id' => '2',
            'title' => $t1,
            'user_id' => '1',
            'content' => 'this is lorem ipsem text here',
            'slug' => str_slug($t2)
        ];

        $d3 = [

            'channel_id' => '3',
            'title' => $t1,
            'user_id' => '1',
            'content' => 'this is lorem ipsem text here',
            'slug' => str_slug($t3)
        ];

        $d4 = [

            'channel_id' => '4',
            'title' => $t1,
            'user_id' => '1',
            'content' => 'this is lorem ipsem text here',
            'slug' => str_slug($t4)
        ];

        $d5 = [

            'channel_id' => '5',
            'title' => $t1,
            'user_id' => '1',
            'content' => 'this is lorem ipsem text here',
            'slug' => str_slug($t5)
        ];

        \App\Discussion::create($d1);
        \App\Discussion::create($d2);
        \App\Discussion::create($d3);
        \App\Discussion::create($d4);
        \App\Discussion::create($d5);
    }
}
